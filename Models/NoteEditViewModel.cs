﻿using NotesApp.Areas.Identity.Data;

namespace NotesApp.Models
{
    public class NoteEditViewModel
    {
        public Guid Id { get; set; }   
        public required string Title { get; set; }
        public required string Content { get; set; }
    }
}