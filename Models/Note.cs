using Microsoft.AspNetCore.Identity;
using NotesApp.Areas.Identity.Data;
using System.ComponentModel.DataAnnotations.Schema;

public class Note
{
    public Guid Id { get; set; }
    public required string Title { get; set; }
    public required string Content { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    public ApplicationUser User { get; set; }
    public string UserId { get; set; }
}
