using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NotesApp.Areas.Identity.Data;
using NotesApp.Data;
using NotesApp.Models;


namespace NotesApp.Controllers;

[Authorize]
public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly NotesAppContext _context;
    private readonly UserManager<ApplicationUser> _userManager;

    public HomeController(ILogger<HomeController> logger, NotesAppContext context, UserManager<ApplicationUser> userManager)
    {
        _logger = logger;
        _context = context;
        this._userManager = userManager;
    }

    [HttpGet]
    public async Task<IActionResult> Index(int page = 1, int pageSize = 10,string search="")
    {
        var userId = _userManager.GetUserId(User);

        if (!search.Equals(""))
        {
           var notes = await _context.Notes
            .Where(n => n.UserId == userId && (n.Title.Contains(search) || n.Content.Contains(search)))
            .OrderByDescending(n => n.Id)
            .ToListAsync();

            var pageCount = notes.Count();

            PaginationViewModel Pagination = new PaginationViewModel
            {
                List = notes,
                NotesSize = pageCount,
                CurrentPage = page
            };

            return View(Pagination);
        }
        else {
            // Calculate the number of items to skip based on the page number and page size
            var skip = (page - 1) * pageSize;

            // Retrieve the notes for the current page from the repository
            var notes = await _context.Notes
                .Where(n => n.UserId == userId)
                .OrderByDescending(n => n.Id)
                .Skip(skip)
                .Take(pageSize)
                .ToListAsync();

            var pageCount = _context.Notes.Count();

            PaginationViewModel Pagination = new PaginationViewModel
            {
                List = notes,
                NotesSize = pageCount,
                CurrentPage = page
            };

            return View(Pagination);
        }  
    }


    [HttpGet]
    public async Task<IActionResult> Edit(Guid id)
    {
        // Retrieve the note with the specified id from the repository
        var note = await _context.Notes.FindAsync(id);

        if (note == null)
        {
            return NotFound();
        }
        var NoteEditViewModel = new NoteEditViewModel
        {
            Id = note.Id,
            Title = note.Title,
            Content = note.Content
        };
        return View(NoteEditViewModel);  
    }

    [HttpPost]
    public async Task<IActionResult> Edit(NoteEditViewModel viewModel)
    {
        if (!ModelState.IsValid)
        {
            var errorMessages = ModelState.Values.SelectMany(v => v.Errors)
                                             .Select(e => e.ErrorMessage)
                                             .ToList();

            ViewBag.ErrorMessages = errorMessages;

            return View(viewModel);
        }

        // Retrieve the original note from the repository
        var originalNote = await _context.Notes.FindAsync(viewModel.Id);

        if (originalNote is null)
        {
            return NotFound();
        }

        // Update the fields of the original note with the values from the posted note, except for the CreatedOn field  
        originalNote.Title = viewModel.Title;
        originalNote.Content = viewModel.Content;
        originalNote.UpdatedAt = DateTime.Now;

        // Update the note in the repository
        await _context.SaveChangesAsync();

        return RedirectToAction("Index","Home");
    }

    [HttpGet]
    public IActionResult Add()
    {
        return View();
    }

    [HttpPost]
    public async Task<IActionResult> Add(NoteAddViewModel viewModel)
    {
        if (!ModelState.IsValid)
        {
            var errorMessages = ModelState.Values.SelectMany(v => v.Errors)
                                             .Select(e => e.ErrorMessage)
                                             .ToList();

            ViewBag.ErrorMessages = errorMessages;

            return View(viewModel);
        }

        var UserId = _userManager.GetUserId(User);


        // Create a new note with the values from the view model
        var note = new Note
        {
            UserId = UserId,
            Title = viewModel.Title,
            Content = viewModel.Content,
            CreatedAt = DateTime.Now,
            UpdatedAt = DateTime.Now
        };

        // Add the note to the repository
        await _context.Notes.AddAsync(note);
        await _context.SaveChangesAsync();

        return RedirectToAction("Index", "Home"); // Redirect to the Index action of the HomeController
    }

    [HttpPost]
    public async Task<IActionResult> Delete(Guid id)
    {
        // Retrieve the note with the specified id from the repository
        var note = await _context.Notes.AsNoTracking().FirstOrDefaultAsync(n => n.Id == id);

        if (note == null)
        {
            return NotFound();
        }

        // Remove the note from the repository
        _context.Notes.Remove(note);
        await _context.SaveChangesAsync();

        return RedirectToAction("Index", "Home");
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
