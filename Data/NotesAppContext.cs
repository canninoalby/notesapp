﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using NotesApp.Areas.Identity.Data;
using System.Reflection.Emit;

namespace NotesApp.Data;

public class NotesAppContext : IdentityDbContext<ApplicationUser>
{
    public NotesAppContext(DbContextOptions<NotesAppContext> options)
        : base(options)
    {
    }

    public DbSet<Note> Notes { get; set; }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        builder.Entity<ApplicationUser>(e =>
        {
            e.HasMany(p => p.Notes)
            .WithOne(p => p.User)
            .HasForeignKey(p => p.UserId);
        });
    }
}
