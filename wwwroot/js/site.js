﻿// Please see documentation at https://learn.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
function setSearchRoute() {
    var searchValue = document.getElementById('searchInput').value;
    window.location.href = '/Home/Index?search=' + encodeURIComponent(searchValue);
}