using NotesApp.Areas.Identity.Data;

namespace NotesApp.Models
{
    public class NoteAddViewModel
    {
        public required string Title { get; set; }
        public required string Content { get; set; }
    }
}