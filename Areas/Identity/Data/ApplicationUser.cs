using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace NotesApp.Areas.Identity.Data
{
    public class ApplicationUser : IdentityUser
    {
        // Add any additional properties or methods here
        public required List<Note> Notes { get; set; }

    }

    
}
