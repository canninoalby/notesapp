﻿namespace NotesApp.Models
{
    public class PaginationViewModel
    {
        public required List<Note> List { get; set; } 
        public int CurrentPage { get; set; }
        public int NotesSize { get; set; }
    }
}
